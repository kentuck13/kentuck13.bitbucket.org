import React from 'react';
import {ServiceBlock} from './styled/ServiceBlock';

const Service = () => (
    <div className="container">
        <div className="row">
            <ServiceBlock>
                <div className="title" data-aos="fade-left" data-aos-delay="150">SERVICE</div>
                <div className="border" data-aos="fade-right" data-aos-delay="200"/>
                <div className="service">
                    <div className="main" data-aos="zoom-in" data-aos-delay="300">
                        <i className="fa fa-html5"/>
                        <div className="name">Front-end</div>
                        <div className="desc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </div>
                    </div>
                    <div className="main" data-aos="zoom-in" data-aos-delay="500">
                        <i className="fa fa-magic"/>
                        <div className="name">Back-end</div>
                        <div className="desc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </div>
                    </div>
                    <div className="main" data-aos="zoom-in" data-aos-delay="700">
                        <i className="fa fa-android"/>
                        <div className="name">Bot</div>
                        <div className="desc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                        </div>
                    </div>

                </div>
            </ServiceBlock>
        </div>
    </div>
);

export default Service;