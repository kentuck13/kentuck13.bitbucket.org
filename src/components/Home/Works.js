import React, {Component} from 'react';
import {WorksBlock} from './styled/WorksStyle';

class Works extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <WorksBlock>
                        <div className="title" data-aos="fade-left" data-aos-delay="150">LAST WORKS</div>
                        <div className="border" data-aos="fade-right" data-aos-delay="200"/>
                        <div className="works">
                            <a href="" className="work"
                               style={{backgroundImage: `url(${require('assets/sam-ferrara-136526.jpg')})`}}>
                                <div>
                                    REAL-HOUSE
                                </div>
                            </a>
                            <a href="" className="work"
                               style={{backgroundImage: `url(${require('assets/sam-ferrara-136526.jpg')})`}}>
                                <div>
                                    REAL-HOUSE
                                </div>
                            </a>
                            <a href="" className="work"
                               style={{backgroundImage: `url(${require('assets/sam-ferrara-136526.jpg')})`}}>
                                <div>
                                    REAL-HOUSE
                                </div>
                            </a>
                            <a href="" className="work"
                               style={{backgroundImage: `url(${require('assets/sam-ferrara-136526.jpg')})`}}>
                                <div>
                                    REAL-HOUSE
                                </div>
                            </a>
                            <a href="" className="work"
                               style={{backgroundImage: `url(${require('assets/sam-ferrara-136526.jpg')})`}}>
                                <div>
                                    REAL-HOUSE
                                </div>
                            </a>
                            <a href="" className="work"
                               style={{backgroundImage: `url(${require('assets/sam-ferrara-136526.jpg')})`}}>
                                <div>
                                    REAL-HOUSE
                                </div>
                            </a>
                        </div>
                        <div className="view-more" data-aos="flip-left" data-aos-delay="200">
                            <a href="">VIEW MORE</a>
                        </div>
                    </WorksBlock>
                </div>
            </div>
        )
    }
}

export default Works;