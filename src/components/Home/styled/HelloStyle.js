import styled from 'styled-components';

export const HelloBlock = styled.div`
    background: url(${require('assets/colin-cassidy-269012.jpg')}) no-repeat center fixed;
    background-size: cover;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #fff;
    position: relative;
    height: 100%;
    &:before{
        content: " ";
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0), rgba(0, 0, 0, 0), rgba(0, 0, 0, 1));
    }
    
    .content{
        position: relative;
        overflow: hidden;
        border-radius: 8px;
        z-index: 2;
        padding-left: 30px;
        padding-right: 30px;
        font-weight: 100;
        line-height: 28px;
        text-align: center;
        padding-bottom: 50px;
        h1{
            margin: 25px 0 10px 0;
            text-transform: uppercase;
            font-size: 33px;
        }
        h4{
            margin: 0;
        }
        
        a{
            color: #fff;
            border: 2px solid #fff;
            text-decoration: none;
            display: inline-block;
            padding: 7px 15px;
            text-transform: uppercase;
            border-radius: 15px;
            line-height: normal;
            font-size: 13px;
            font-weight: 100;
            &:hover{
                background: #fff;
                color: #000;
            }
        }
    }
`;