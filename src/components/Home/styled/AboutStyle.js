import styled from 'styled-components';

export const AboutBlock = styled.div`
    color: #fff;
    height: 100%;
    .content{
        height: 100%;
        display: flex;
        justify-content: space-between;
        align-items: center;
        flex-flow: row wrap;
        .txt{
            width: 45%;
            line-height: 20px;
            font-size: 14px;
            .title{
                font-size: 33px;
                position: relative;   
            }
            .border{
                width: 100px;
                height: 2px;
                background: #9a9a9a;
                margin-top: 13px;
                margin-bottom: 30px;
            }
            .hello{
                font-size: 18px;
            }
            
            a{
                padding: 7px 10px;
                border: 2px solid #fff;
                display: inline-block;
                color: #fff;
                margin-right: 15px;
                text-decoration: none;
                margin-top: 40px;
                &.hire{
                    background: #fff;
                    color: #000;
                }
            }   
        }
        .skills{
            width: 45%;
            line-height: 20px;
            .direction{
                font-size: 16px;
                text-transform: uppercase;
                margin-bottom: 3px;
                margin-top: 15px;
            }
            a{
                margin-left: 10px;
                color: #fff;
                display: inline-block;
            }
        }
    }
`;