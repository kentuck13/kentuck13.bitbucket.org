import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import routers from './routers';
import 'theme/global';
import './theme/normalize.css';
import aos from 'aos/dist/aos';
import 'aos/dist/aos.css';

aos.init();

ReactDOM.render(
    routers,
    document.getElementById('root'),
);

// render(routers);
// if (module.hot) {
//     module.hot.accept('./routers', () => {
//         render(routers)
//     })
// }
registerServiceWorker();
