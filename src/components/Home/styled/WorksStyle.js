import styled from 'styled-components';

export const WorksBlock = styled.div`
    color: #fff;
    .title{
        font-size: 33px;
        position: relative;   
    }
    .border{
        width: 100px;
        height: 2px;
        background: #9a9a9a;
        margin-top: 13px;
        margin-bottom: 30px;
    }
    
    .works{
        display: flex;
        justify-content: space-between;
        flex-flow: row wrap;
        .work{
            width: 370px;
            height: 180px;
            margin-bottom: 30px;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-flow: row wrap;
            overflow: hidden;
            position: relative;
            transition: box-shadow .35s;
            text-decoration: none;
            div{
                color: #fff;
            }
            &:before, &:after{
                content: " ";
                opacity: 0;
                position: absolute;
                transition: opacity 0.35s, transform 0.35s;
            }
            &:before{
                top: 30px;
                right: 10px;
                bottom: 30px;
                left: 10px;
                border-top: 1px solid #fff;
                border-bottom: 1px solid #fff;
                transform: scale(0,1);
                transform-origin: 0 0;
            }
            &:after{
                top: 10px;
                right: 30px;
                bottom: 10px;
                left: 30px;
                border-right: 1px solid #fff;
                border-left: 1px solid #fff;
                transform: scale(1,0);
                transform-origin: 100% 0;
            }
            &:hover{
                box-shadow: inset 0 0 0 3000px rgba(0,0,0, .8);
                &:before, &:after{
                    opacity: 1;
                    transform: scale(1);
                }
            }
        }
    }
    
    .view-more{
        width: 100%;
        text-align: center;
        a{
            color: #fff;
            text-decoration: none;
            text-transform: uppercase;
            border: 2px solid #fff;
            display: inline-block;
            padding: 5px 7px;
            transition: all .5s;
            &:hover{
                background: #fff;
                color: #000;
            }
        }
    }
`;