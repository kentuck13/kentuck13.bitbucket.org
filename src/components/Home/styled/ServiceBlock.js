import styled from 'styled-components';

export const ServiceBlock = styled.div`
    color: #fff;
    .title{
        font-size: 33px;
        position: relative;   
    }
    .border{
        width: 100px;
        height: 2px;
        background: #9a9a9a;
        margin-top: 13px;
        margin-bottom: 30px;
    }
    
    .service{
        margin-top: 80px;
        display: flex;
        justify-content: space-between;
        flex-flow: row wrap;
        text-align: center;
        .main{
            width: 300px;
            border: 2px solid #fff;
            padding: 30px;
            cursor: pointer;
            i{
                font-size: 37px;
            }
            .name{
                font-size: 20px;
                margin-top: 15px;
                margin-bottom: 15px;
            }
            .desc{
                font-size: 14px;
                line-height: 26px;
            }
            &:nth-child(even){
                color: #000;
                background: #fff;
            }
            &:hover{
                color: #000;
                background: #fff;
            }   
        }
    }
`;