import React, {Component} from 'react';
import Hello from './Hello';
import About from './About';
import Experience from './Experience';
import Works from './Works';
import Contacts from './Contacts';
import Service from './Service';
import $ from 'jquery';
import 'fullpage.js';
import 'fullpage.js/dist/jquery.fullpage.css';


class Home extends Component {
    componentDidMount() {
        $('#fullpage').fullpage({
            anchors: ['Hello', 'AboutMe', 'Experience', 'Service', "LastWorks", 'Contact'],
            css3: false,
            navigation: true,
            navigationPosition: 'right',
            navigationTooltips: ['Hello', 'About Me', 'Experience', 'Service', "Last works", 'Contact'],
            scrollBar: true,
            sectionsColor: ['#000', '#000', '#000', '#000', '#000', '#000'],
            menu: ".global-menu__wrap",
        });
    }

    render() {
        return (
            <div id="fullpage">
                <div className="section">
                    <Hello/>
                </div>
                <div className="section">
                    <About/>
                </div>
                <div className="section">
                    <Experience/>
                </div>
                <div className="section">
                    <Service/>
                </div>
                <div className="section">
                    <Works/>
                </div>
                <div className="section">
                    <Contacts/>
                </div>
            </div>
        )
    }
}

export default Home;