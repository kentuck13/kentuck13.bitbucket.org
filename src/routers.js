import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import PrimaryLayout from 'containers/PrimaryLayout';

export default (
    <BrowserRouter>
        <PrimaryLayout/>
    </BrowserRouter>
)