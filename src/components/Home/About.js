import React, {Component} from 'react';
import {AboutBlock} from './styled/AboutStyle';


class About extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <AboutBlock>
                        <div className="content">
                            <div className="txt" data-aos="fade-right" data-aos-delay="100">
                                <div className="title" data-aos="fade-left" data-aos-delay="150">ABOUT ME</div>
                                <div className="border" data-aos="fade-right" data-aos-delay="200"/>
                                <div className="hello">Hello I'm ALEXANDER KIM</div>
                                <p>
                                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam cumque ducimus pariatur praesentium? Assumenda beatae culpa eius, eveniet ex facere id molestias nam perspiciatis quia quis quod rem repellat ut.</span><span>Architecto commodi consequatur consequuntur corporis cum dignissimos dolore dolores doloribus, in incidunt inventore ipsa ipsum iste, laboriosam laborum nam nesciunt nulla optio possimus quam quo soluta tempora unde veniam veritatis!</span><span>Ad beatae dolorem doloremque eveniet laudantium, reprehenderit velit. Deserunt dolorem doloribus explicabo facilis ipsum nesciunt nostrum, officiis quibusdam repellat sapiente tenetur ullam. Accusantium, culpa dolore sint tempore vero vitae voluptates!</span>
                                </p>
                                <a href="" className="hire">HIRE ME</a>
                                <a href="" className="cv">DOWNLOAD CV</a>
                            </div>
                            <div className="skills" data-aos="fade-left" data-aos-delay="100">
                                <div className="direction">Back-end:</div>
                                <a href="">PHP</a>,
                                <a href="">Laravel</a>,
                                <a href="">Python</a>,
                                <a href="">Django</a>
                                <div className="direction">Front-end:</div>
                                <a href="">HTML</a>,
                                <a href="">CSS</a>,
                                <a href="">JavaScript</a>,
                                <a href="">ES6</a>
                                <a href="">Jquery</a>,
                                <a href="">React&Redux</a>,
                                <a href="">bootstrap</a>,
                                <a href="">flexbox</a>
                                <div className="direction">Tools:</div>
                                <a href="">Linux</a>,
                                <a href="">LEMP</a>,
                                <a href="">apache</a>,
                                <a href="">composer</a>,
                                <a href="">pip</a>,
                                <a href="">GIT</a>,
                                <a href="">webpack</a>
                                <a href="">npm</a>,
                                <a href="">yarn</a>
                                <a href="">PhotoShop</a>
                                <a href="">Sketch</a>
                                <a href="">Jetbrains</a>
                            </div>
                        </div>
                    </AboutBlock>
                </div>
            </div>
        )
    }
}

export default About;