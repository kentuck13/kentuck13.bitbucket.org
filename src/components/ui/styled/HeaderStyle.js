import styled from 'styled-components';

export const HeaderBlock = styled.header`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 999;
    .wrap{
        margin-top: 10px;
        color: #fff;
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 24px;
        a{
            color: #fff;
            z-index: 999;
        }
    }
`;