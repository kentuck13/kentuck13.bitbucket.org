import React, {Component} from 'react';
import {HeaderBlock} from './styled/HeaderStyle';
import 'theme/demo.css';
import {ShapeOverlays} from "./_header_help";
import $ from 'jquery';

class Header extends Component {
    componentDidMount() {
        const elmHamburger = document.querySelector('.hamburger');
        const gNavItems = document.querySelectorAll('.global-menu__item');
        const elmOverlay = document.querySelector('.shape-overlays');
        const overlay = new ShapeOverlays(elmOverlay);
        $('.global-menu__item').click((e) => {
            e.preventDefault();
            elmHamburger.click();
            let data = $(e.target).attr('data-menuanchor');
            setTimeout(() => {
                $.fn.fullpage.moveTo(data);
            }, 900);
        });
        elmHamburger.addEventListener('click', (e) => {
            e.preventDefault();
            if (overlay.isAnimating) {
                return false;
            }
            overlay.toggle();
            if (overlay.isOpened === true) {
                elmHamburger.classList.add('is-opened-navi');
                elmHamburger.classList.add('tcon-transform');
                for (let i = 0; i < gNavItems.length; i++) {
                    gNavItems[i].classList.add('is-opened');
                }
            } else {
                elmHamburger.classList.remove('is-opened-navi');
                elmHamburger.classList.remove('tcon-transform');
                for (let i = 0; i < gNavItems.length; i++) {
                    gNavItems[i].classList.remove('is-opened');
                }
            }
        });

    }

    render() {
        return (
            <div>
                <HeaderBlock>
                    <div className="container">
                        <div className="row">
                            <div className="wrap">
                                <div className="resume">RESUME</div>
                                <button type="button" className="hamburger tcon tcon-menu--xbutterfly"
                                        aria-label="toggle menu">
                                    <span className="tcon-menu__lines" aria-hidden="true"/>
                                    <span className="tcon-visuallyhidden">toggle menu</span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div className="global-menu">
                        <div className="global-menu__wrap">
                            <a data-menuanchor="Hello" className="global-menu__item global-menu__item--demo-3"
                               href="#Hello">Hello</a>
                            <a data-menuanchor="AboutMe" className="global-menu__item global-menu__item--demo-3"
                               href="#AboutMe">About</a>
                            <a data-menuanchor="Experience" className="global-menu__item global-menu__item--demo-3"
                               href="#Experience">Experience</a>
                            <a data-menuanchor="Service" className="global-menu__item global-menu__item--demo-3"
                               href="#Service">Service</a>
                            <a data-menuanchor="LastWorks" className="global-menu__item global-menu__item--demo-3"
                               href="#LastWorks">Last Works</a>
                            <a className="global-menu__item global-menu__item--demo-3"
                               href="">Works</a>
                            <a data-menuanchor="Contact" className="global-menu__item global-menu__item--demo-3"
                               href="#Contact">Contact</a>
                        </div>
                    </div>
                    <svg className="shape-overlays" viewBox="0 0 100 100" preserveAspectRatio="none">
                        <path className="shape-overlays__path"/>
                        <path className="shape-overlays__path"/>
                        <path className="shape-overlays__path"/>
                    </svg>
                </HeaderBlock>
            </div>
        )
    }
}

export default Header;