import styled from 'styled-components';

export const ContactsBlock = styled.div`
    color: #fff;
    .title{
        font-size: 33px;
        position: relative;   
    }
    .border{
        width: 100px;
        height: 2px;
        background: #9a9a9a;
        margin-top: 13px;
        margin-bottom: 30px;
    }
    
    .contacts{
        display: flex;
        align-items: center;
        flex-flow: row wrap;
        
        .left{
            margin-left: 80px;
            .title{
                font-size: 20px;
                font-weight: 500;
                margin-bottom: 7px;
                margin-top: 30px;
            }
            
            a{
                font-size: 16px;
                color: #9a9a9a;
                text-decoration: none;
                font-weight: 0;
                margin-bottom: 3px;
                display: inline-block;
            }
            
            .socials{
                margin-top: 50px;
                a{
                    margin-right: 15px;
                    color: #fff;
                    font-size: 24px;
                }
            }
        }
        .middle{
            width: 2px;
            height: 350px;
            background: #9a9a9a;
            margin-right: 100px;
            margin-left: 100px;
        }
        .right{
            width: 500px;
            .title{
                margin-bottom: 40px;
                font-size: 24px;
                color: #9a9a9a;
            }
            form{
                input, button{
                    display: block;
                    border: none;
                    background: transparent;
                    color: #fff;
                    font-size: 17px;
                    padding-bottom: 7px;
                    margin-bottom: 30px;
                    outline: none;
                }
                input{
                    border-bottom: 2px solid #9a9a9a;
                    width: 100%;
                    &::placeholder{
                        color: #fff;
                    }   
                }
                button{
                    border: 2px solid #9a9a9a;
                    width: 120px;
                    height: 40px;
                    line-height: 33px;
                    cursor: pointer;
                    transition: all .35s;
                    &:hover{
                        background: #fff;
                        color: #000;
                    }
                }
            }
        }
    }
`;