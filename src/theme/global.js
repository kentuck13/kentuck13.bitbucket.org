import {injectGlobal} from 'styled-components';

injectGlobal`
@import url('https://fonts.googleapis.com/css?family=Montserrat');
@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css);

body{
    margin: 0;
    padding: 0;
    font-family: 'Montserrat', sans-serif;
}

.container{
    max-width: 1200px;
    width: 100%;
    margin: 0 auto;
}

.row{
    margin-left: 15px;
    margin-right: 15px;
}

#fp-nav ul li a span, .fp-slidesNav ul li a span{
    background: #fff;
}
`;