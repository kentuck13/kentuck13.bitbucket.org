import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom'
import Home from 'containers/Home';
import Header from 'components/ui/Header';


class PrimaryLayout extends Component {
    render() {
        return (
            <div className="primary-layout">
                <Header/>
                <main>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                    </Switch>
                </main>
            </div>
        )
    }
}

export default PrimaryLayout