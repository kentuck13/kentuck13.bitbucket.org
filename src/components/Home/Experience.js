import React, {Component} from 'react';
import {ExperienceBlock} from './styled/ExperienceStyle';

class Experience extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <ExperienceBlock>
                        <div className="title">
                            <div data-aos="fade-left" data-aos-delay="150">EXPERIENCE</div>
                            <div className="border" data-aos="fade-right" data-aos-delay="100"/>
                        </div>
                        <div className="exp" >
                            <div className="name-date">
                                <a href="">Behance</a>
                                <div className="date">
                                    2017-2018
                                </div>
                            </div>
                            <div className="position">
                                <div className="success">
                                    <i className="fa fa-check"/>
                                </div>
                                <div className="line" data-aos="fade-down" data-aos-delay="250"/>
                                <div className="name">Back-end Developer</div>
                                <div className="desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                </div>
                            </div>
                        </div>
                        <div className="exp">
                            <div className="name-date">
                                <a href="">Behance</a>
                                <div className="date">
                                    2017-2018
                                </div>
                            </div>
                            <div className="position">
                                <div className="success">
                                    <i className="fa fa-check"/>
                                </div>
                                <div className="line" data-aos="fade-down" data-aos-delay="450"/>
                                <div className="name">Back-end Developer</div>
                                <div className="desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                </div>
                            </div>
                        </div>
                        <div className="exp">
                            <div className="name-date">
                                <a href="">Behance</a>
                                <div className="date">
                                    2017-2018
                                </div>
                            </div>
                            <div className="position">
                                <div className="success">
                                    <i className="fa fa-check"/>
                                </div>
                                <div className="name">Back-end Developer</div>
                                <div className="desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                </div>
                            </div>
                        </div>
                        <div className="exp">
                            <div className="name-date">
                                <a href="">Behance</a>
                                <div className="date">
                                    2017-2018
                                </div>
                            </div>
                            <div className="position">
                                <div className="success">
                                    <i className="fa fa-check"/>
                                </div>
                                <div className="name">Back-end Developer</div>
                                <div className="desc">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                </div>
                            </div>
                        </div>
                        <div className="view-more" data-aos="flip-left" data-aos-delay="300"><a href="">see more</a>
                        </div>
                    </ExperienceBlock>
                </div>
            </div>
        )
    }
}

export default Experience;