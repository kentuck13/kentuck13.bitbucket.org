import React, {Component} from 'react';
import {ContactsBlock} from './styled/ContactsStyle';

class Contacts extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                    <ContactsBlock>
                        <div className="title" data-aos="fade-left" data-aos-delay="150">CONTACT</div>
                        <div className="border" data-aos="fade-right" data-aos-delay="200"/>
                        <div className="contacts">
                            <div className="left" data-aos="fade-right" data-aos-delay="200">
                                <div className="title">Email:</div>
                                <a href="">kentuck465@gmail.com</a> <br/>
                                <a href="">kentuck13@gmail.com</a> <br/>
                                <div className="title">Address:</div>
                                <a href="">100 Banani Road, House no 4 <br/>
                                    Utrra Dhaka, Ukrain
                                </a>

                                <div className="socials">
                                    <a href=""><i className="fa fa-facebook"/></a>
                                    <a href=""><i className="fa fa-github"/></a>
                                </div>
                            </div>
                            <div className="middle"  data-aos="fade-down" data-aos-delay="450"/>
                            <div className="right" data-aos="fade-left" data-aos-delay="200">
                                <div className="title">Feel Free to Knock me</div>
                                <form action="">
                                    <input type="text" placeholder="Name"/>
                                    <input type="email" placeholder="Email"/>
                                    <input type="text" placeholder="Message"/>
                                    <button>SEND</button>
                                </form>
                            </div>
                        </div>
                    </ContactsBlock>
                </div>
            </div>
        )
    }
}

export default Contacts;