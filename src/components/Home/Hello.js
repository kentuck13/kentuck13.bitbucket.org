import React, {Component} from 'react';
import {HelloBlock} from './styled/HelloStyle';


class Hello extends Component {

    render() {
        return (
            <HelloBlock>
                <div className='content'>
                    <h1 data-aos='flip-left'>I am Alexander Kim</h1>
                    <p>Hello i am Alexander passionate back-end and front-end <br/>developer.</p>
                    <a href="">Show portfolio</a>
                </div>
            </HelloBlock>
        )
    }
}

export default Hello;