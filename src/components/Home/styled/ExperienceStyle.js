import styled, {keyframes} from 'styled-components';

const grow = keyframes`
	0% {
		filter: blur(0px);
	}
	100% {
		filter: blur(17px);
	}
`;
export const ExperienceBlock = styled.div`
    color: #fff;
    display: flex;
    justify-content: space-between;
    align-items: stretch;
    flex-flow: row wrap;
    height: 100%;
    .title{
        width: 100%;
        font-size: 36px;
        margin-bottom: 100px;
        .border{
            margin-top: 13px;
            width: 100px;
            height: 2px;
            background: #9a9a9a;
        }
    }
    .exp{
        display: flex;
        justify-content: center;
        flex-flow: row wrap;
        height: 150px;
        .name-date{
            width: 150px;
            font-weight: 100;
            font-size: 14px;
            a{
                color: #fff;
                font-size: 22px;
                margin-bottom: 4px;
                display: block;
            }
        }    
        .position{
            width: 360px;
            padding-left: 50px;
            position: relative;
            .success{
                position: absolute;
                top: 0;
                left: 0;
                width: 25px;
                height: 25px;
                background: #fff;
                border-radius: 50%;
                color: #000;
                text-align: center;
                line-height: 25px;
                font-size: 12px;
                z-index: 2;
            }
            .line{
                    width: 2px;
                    height: 100%;
                    background: #9a9a9a;
                    position: absolute;
                    top: 0;
                    left: 12px;
                }
            .name{
                font-size: 24px;
                margin-bottom: 8px;
            }
            .desc{
                font-size: 18px;
                letter-spacing: 1.1;
            }
        }
    }
    .view-more{
        width: 100%;
        text-align: center;
        a{
            color: #fff;
            text-decoration: none;
            text-transform: uppercase;
            border: 2px solid #fff;
            display: inline-block;
            padding: 5px 7px;
            transition: all .5s;
            &:hover{
                background: #fff;
                color: #000;
            }
        }
    }
`;